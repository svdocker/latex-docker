FROM debian:bullseye-slim

ARG USER_NAME=latex
ARG USER_HOME=/home/latex
ARG USER_ID=1000
ARG USER_GECOS=LaTeX

RUN adduser \
  --home "${USER_HOME}" \
  --uid "${USER_ID}" \
  --gecos "${USER_GECIS}" \
  --disabled-password \
  "${USER_NAME}"

RUN DEBIAN_FRONTEND=noninteractive \
  apt-get update && \
  apt-get -y install \
  wget git openssh-client make \
  pandoc pandoc-citeproc \
  fig2dev \
  tidy \
  imagemagick potrace \
  pdf2svg \
  zip \
  python3 python3-pip && \
  apt-get clean -y && \
  pip3 install Pygments && \
  ln -s /usr/bin/python3 /usr/bin/python

COPY texlive.profile /tmp

ENV PATH="/usr/local/texlive/2020/bin/x86_64-linux/:$PATH"

RUN cd /tmp && \
  wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz && \
  DIR=$(tar xvf install-tl-unx.tar.gz | tail -1 | awk 'BEGIN { FS="/" }; {print $1}') && \
  cd "${DIR}" && \
  perl install-tl -profile /tmp/texlive.profile && \
  wget https://raw.githubusercontent.com/gsalzer/subfiles/v2.1/subfiles.sty -O /usr/local/texlive/2020/texmf-dist/tex/latex/subfiles/subfiles.sty && \
  wget https://raw.githubusercontent.com/gsalzer/subfiles/v2.1/subfiles.cls -O /usr/local/texlive/2020/texmf-dist/tex/latex/subfiles/subfiles.cls && \
  /usr/local/texlive/2020/bin/x86_64-linux/texhash && \
  /usr/local/texlive/2020/bin/x86_64-linux/luaotfload-tool --update --force --verbose=1 && \
  wget https://raw.githubusercontent.com/michal-h21/make4ht/master/extensions/make4ht-ext-staticsite.lua \
       -O /usr/local/texlive/2020/texmf-dist/scripts/make4ht/extensions/make4ht-ext-staticsite.lua

COPY ./docbld-0.1.1-py3-none-any.whl /tmp/docbld-0.1.1-py3-none-any.whl 

RUN pip3 install /tmp/docbld-0.1.1-py3-none-any.whl 
